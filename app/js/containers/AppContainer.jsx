import React, { PureComponent } from 'react'
import Table from 'components/Table'

class AppContainer extends PureComponent {
  render(){
    return <>
    <Table />
    </>
  }
}

export default AppContainer
