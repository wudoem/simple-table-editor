import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledCell = styled.div`
  color: rgba(0, 0, 0, 0.7);
  border-right: 1px solid rgba(0, 0, 0, 0.2);
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  min-height: 21px;
  position: relative;
  padding: 5px;
  display: flex;
  flex: 1;
  align-items: center;
`
const Input = styled.input`
  width: 100%;
`
export const RemoveButton = styled.span`
  display: block;
  font-weight: bold;
  right: 5px;
  top: 0;
  bottom: 0;
  z-index: 10;
  cursor: pointer;
  margin-left: 5px;
`
const Flex = styled.div`
  display: flex;
  flex: 1;
`

const Cell = ({ data, onClick, editMode, onChange, onTab, onRemove }) => {
  return <StyledCell onClick={onClick}>
    {editMode && <Flex>
      <Input type="text" value={data || ''} onChange={onChange} onKeyDown={onTab} autoFocus />{(typeof onRemove !== 'undefined') && <RemoveButton onClick={onRemove}>❌</RemoveButton>}
    </Flex> || data}
  </StyledCell>
}

Cell.propTypes = {
  data: PropTypes.any,
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
  onTab: PropTypes.func,
  editMode: PropTypes.bool,
}

export default Cell
