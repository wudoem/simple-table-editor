
import React, { PureComponent } from 'react'

import Cell, { RemoveButton } from './Cell'
import styled, { css } from 'styled-components'

const InlineFlex = css`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  flex: 1;
  margin: 5px 0;
`
const ColumnsWrapper = styled.div`
  ${InlineFlex}
  background: rgba(0, 0, 0, 0.1);
  font-size: 17px;
  margin: 5px 25px;
  margin-left: 0;
`
const RowsWrapper = styled.div`
  ${InlineFlex}
  font-size: 14px;
  position: relative;
`
const Button = styled.button`
  padding: 5px 15px;
  background: white;
  border-radius: 3px;
  border: 1px solid rgba(0, 0, 0, 0.3);
  outline: none;
  margin-right: 5px;
  cursor: pointer;

  :last-child {
    margin-right: 0;
  }

  &:disabled {
    cursor: not-allowed;
  }
`

class Table extends PureComponent {
  state = {
    columns: new Array(5).fill('', 0, 5),
    rows: [],
    col: -1,
    row: -1
  }

  addRow = () => {
    const rows = this.state.rows.map(item => item)
    rows.push(new Array(this.state.columns.length).fill('', 0, this.state.columns.length))

    this.setState({ rows })
  }

  addColumn = () => {
    this.setState({
      columns: this.state.columns.concat('')
    })
  }

  onCellSelect = ({ col, row }) => {
    this.setState({ col, row })
  }

  onCellEdit = ({ e, col, row }) => {
    e.preventDefault()

    if (typeof row !== 'undefined') {
      const rows = this.state.rows.map(item => item)
      rows[row][col] = e.target.value

      this.setState({ ...this.state, rows })
      return
    }

    const columns = this.state.columns.map(item => item)
    columns[col] = e.target.value

    this.setState({ columns })
  }

  onTabKeyDown = (e) => {
    if (e.key === "Tab") {
      e.preventDefault();
      this.setState({
        col: ((this.state.columns.length > this.state.col + 1) && this.state.col + 1) || -1,
      })
    }
  }

  onColumnRemove = ({ col }) => {
    this.setState({ columns: this.state.columns.filter((_, k) => k !== col) })
  }

  onRowRemove = ({ row }) => {
    this.setState({ rows: this.state.rows.filter((_, key) => key !== row) })
  }

  fillCSVfields = (field) => {
    return field.length && field || ","
  }

  getCSV = () => {
    const { rows, columns } = this.state
    const result = `${columns.map(this.fillCSVfields).join(',')}
${rows.map(item => item.map(this.fillCSVfields)).join('\r\n')}`

    const file = new Blob([result], { encoding: "UTF-8", type: "text/csv;charset=UTF-8" })
    const tempLink = document.createElement('a');
    tempLink.href = window.URL.createObjectURL(file)
    tempLink.setAttribute('download', `${new Date()}.csv`)
    tempLink.click()
  }

  render() {
    const { columns, rows } = this.state
    const canDownloadFile = this.state.columns.filter(item => item.length).length === this.state.columns.length

    return <>
      <div>
        <Button onClick={this.addColumn}>Dodaj kolumnę</Button>
        <Button onClick={this.getCSV} disabled={!canDownloadFile}>Pobierz CSV</Button>
      </div>
      {<ColumnsWrapper>
        {columns
          .map((data, col) => <Cell onTab={this.onTabKeyDown} editMode={this.state.col === col && typeof this.state.row === 'undefined'} onRemove={() => this.onColumnRemove({ col })} onChange={(e) => this.onCellEdit({ e, col })} data={data} key={col} onClick={() => this.onCellSelect({ col })} />)}
      </ColumnsWrapper>}

      {rows.length > 0 && rows.map((row, rowKey) => <RowsWrapper key={rowKey}>
        {columns.map((item, colKey) => <Cell onTab={this.onTabKeyDown} remove={false} key={colKey} data={rows[rowKey][colKey]} editMode={this.state.row === rowKey && this.state.col === colKey} onChange={(e) => this.onCellEdit({ e, col: colKey, row: rowKey })} onClick={() => this.onCellSelect({ row: rowKey, col: colKey })} />)}
        <RemoveButton onClick={(e) => this.onRowRemove({ e, row: rowKey })}>❌</RemoveButton>
      </RowsWrapper>
      )}
      <Button onClick={this.addRow}>Dodaj wiersz</Button>
    </>
  }
}

export default Table
