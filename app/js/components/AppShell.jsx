import React, { PureComponent } from 'react'

class AppShell extends PureComponent {
  componentDidMount() {}

  render() {
    return <>{this.props.children}</> // eslint-disable-line
  }
}

export default AppShell
