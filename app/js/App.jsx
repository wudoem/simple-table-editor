import React from 'react'
import { hot } from 'react-hot-loader'

import 'root/css/reset.min.css'
import 'root/css/styles.css'

import AppShell from 'components/AppShell'
import AppContainer from 'containers/AppContainer'

const App = () => (
  <AppShell>
    <AppContainer />
  </AppShell>
)

export default (process.env.NODE_ENV === 'development') ? hot(module)(App) : App
