import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

const init = (element) => {
  try {
    const mount = document.getElementById(element)
    ReactDOM.render(<App />, mount)
  } catch (e) {
    console.error(e)
  }
}

document.addEventListener('DOMContentLoaded', () => {
  init('root')
})
